package com.example.rany.socialtest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.logging.Handler;

public class MainActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private LoginButton loginButton;
    public static final String TAG = "oooooo";
    private AccessToken accessToken;
    private boolean isLoggedIn;
    private Button shareLink, sharePhoto;
    private EditText tag, link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        loginButton.setReadPermissions(Arrays.asList(EMAIL));

        accessToken = AccessToken.getCurrentAccessToken();
        isLoggedIn = accessToken != null && !accessToken.isExpired();
        Log.e(TAG, "isLoggedIn "+ isLoggedIn );


        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(currentAccessToken == null){
                    shareLink.setVisibility(View.GONE);
                    sharePhoto.setVisibility(View.GONE);
                    tag.setVisibility(View.GONE);
                    link.setVisibility(View.GONE);
                }
                else{
                    shareLink.setVisibility(View.VISIBLE);
                    sharePhoto.setVisibility(View.VISIBLE);
                    tag.setVisibility(View.VISIBLE);
                    link.setVisibility(View.VISIBLE);
                }
            }
        };

        shareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog shareDialog = new ShareDialog(MainActivity.this);
                if(ShareDialog.canShow(ShareLinkContent.class)){
                    ShareLinkContent content = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(link.getText().toString()))
                            .setShareHashtag(new ShareHashtag.Builder()
                            .setHashtag("#"+tag.getText().toString()).build())
                            .build();
                    shareDialog.show(content);
                }
            }
        });

        sharePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog shareDialog = new ShareDialog(MainActivity.this);
                if(ShareDialog.canShow(SharePhotoContent.class)){
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                            R.drawable.image);
                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(bitmap)
                            .build();

                    SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();
                    shareDialog.show(sharePhotoContent);
                    }
            }
        });

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "onSuccess: "+ loginResult.getAccessToken());
                getFacebookInfor(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "onCancel: ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "onError: "+ error.getMessage());
            }
        });
    }

    private void getFacebookInfor(AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String name = object.getString("name");
                            String email = object.getString("email");
                            String pic = "https://graph.facebook.com/"+
                                    object.getString("id")+"/picture?type=large";
                            Log.e(TAG, "onCompleted: "+ name );
                            Log.e(TAG, "onCompleted: "+ email);
                            Log.e(TAG, "onCompleted: "+ pic);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView() {
        loginButton = findViewById(R.id.login_button);
        shareLink = findViewById(R.id.shareLink);
        sharePhoto = findViewById(R.id.sharePhoto);
        tag = findViewById(R.id.edtTag);
        link = findViewById(R.id.edtLink);
        // create a callbackManager to handle login responses by calling
        callbackManager = CallbackManager.Factory.create();
    }
}
